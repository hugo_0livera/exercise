#!/bin/bash

echo "###################################################" 
echo "1.Dato un indirizzo di rete (www.google.com)"
	echo "a. Verificare che sia raggiungibile"
	ping www.google.com
	echo "#####################################################"

	echo "(TODO) b.Verificare che risponda sulla porta 443 e non sulla 1234"
	echo "#######################################################"

	echo "(TODO) c.ottenere l'IP con il quale è stato tradotto"
	echo "######################################################"


	echo " d.tracciare la sequenza di router attraversati per raggiungerlo"
	tracert www.google.com
	echo "################################################################"

	echo "4. Scaricare il contenuto di una pagina web (www.google.com) utilizzando tutti i comandi conosciuti"
	curl -o fileGoogle http://google.com
	wget --mirror --convert-links --page-requisites --no-parent -P /home/mobaxterm/Desktop/blueReply  https://google.it
	echo "##################################################"

echo "##################################################################################################"
echo "8.Creare un nuovo repositorio Git"
echo "##################################################################################################"
mkdir newDirGit
cd newDirGit
git init

	echo "##################################################################################################"
	echo "a.Creare un nuovo file al suo interno che contenga la data di oggi nel formato 'YYYYMMDD'"
	echo "##################################################################################################"
	touch newGitFile.txt 
 	date "+%Y%m%d"  >> newGitFile.txt

 	echo "##################################################################################################"
	echo "b.Verificare lo stato del repository"
	echo "##################################################################################################"
	git status
	
	echo "##################################################################################################"
	echo "c. Aggiungere il file creato nell'area di staging"
	echo "##################################################################################################"
	git add newGitFile.txt

	echo "##################################################################################################"
	echo "d. Verificare nuovamente lo stato del repository"
	echo "##################################################################################################"
	git status

	echo "##################################################################################################"
	echo "e. Effetuare il commit del file con il seguente commento 'il mio primo commit'"
	echo "##################################################################################################"
	git config --global user.email "vhugocxd@gmail.com"
	git config --global user.name "Victor Olivera"
	git commit -m "il mio primo commit"

	echo "##################################################################################################"
	echo "f. Elencare tutti i commit effetuati"
	echo "##################################################################################################"
	echo "git log "

	echo "##################################################################################################"
	echo "g. Aggiungere il seguente repository remoto 'https:about.gitlab.com/pricing/#saas'"
	echo "##################################################################################################"
	mkdir prima-repository 
	cd prima-repository 
	git clone git@gitlab.com:hugo_0livera/repositoryblureply.git

	echo "##################################################################################################"
	echo "h. Effettuare l'upload dei commit in remoto"
	echo "##################################################################################################"
	cd ..
	 mv newGitFile.txt /home/mobaxterm/Desktop/blueReply/newDirGit/prima-repository/repositoryblureply
	 cd prima-repository/repositoryblureply
	git add newGitFile.txt
	git commit -m "primo commit in repository"
	git push origin master
	
	
	echo "##################################################################################################"
	echo "i. Modificare i file precedentemente creato cambiando il contenuto in 'YYYY--MM--DD'"
	echo "##################################################################################################"
	date "+%Y--%m--%d"  >> newGitFile.txt

	echo "##################################################################################################"
	echo "j. Mostrare le differenze con il commit precedente"
	echo "##################################################################################################"
	echo "git diff"

	echo "##################################################################################################"
	echo "k. Eseguire il commit delle modifiche"
	echo "##################################################################################################"
	git add newGitFile.txt
	git commit -m "secondo commit"
	git push origin master

	echo "##################################################################################################"
	echo "l. Modificare nuovamente il contenuto del file da un altro repository cambiando il contenuto in 'DD/MM/YYYY'"
	echo "##################################################################################################"

		echo "##################################################################################################"
		echo " i. clonare il repository int una nuova cartella"
		echo "##################################################################################################"
		cd ../..
		mkdir secondaRepository
		cd secondaRepository
		git clone git@gitlab.com:hugo_0livera/repositoryblureply.git

		echo "##################################################################################################"
		echo " ii. effetuare la modifica"
		echo "##################################################################################################"
		cd repositoryblureply
		date "+%Y/%m/%d"  >> newGitFile.txt

		echo "##################################################################################################"
		echo " iii. eseguire commit e push"
		echo "##################################################################################################"
		git add newGitFile.txt
		git commit -m "Terzo commit"
		git push origin master

	echo "##################################################################################################"
	echo "m. Aggiornare il repository remoto con le modifiche locali"
	echo "##################################################################################################"

	echo "##################################################################################################"
	echo "n. Creare un nuovo branch denominato 'new-branch'"
	git branch new-branch
	echo "##################################################################################################"

	echo "##################################################################################################"
	echo "o. Spostarsi sul branch appena creato"
	git checkout new-branch
	git pull origin new-branch
	echo "##################################################################################################"

	echo "##################################################################################################"
	echo "p. Creare nuovi file file e/o cartelle a piacere"
	touch file.txt 
 	echo ciao sono nel nuovo branch  >> file.txt
		git add file.txt
		git commit -m "primo commit nel nuovo branch"
		git push origin new-branch
	echo "##################################################################################################"

	echo "##################################################################################################"
	echo "q. Unire le modifiche dei due branch sul branch master"
	git checkout master
	git merge new-branch
	git push origin master
	echo "##################################################################################################"

	echo "##################################################################################################"
	echo "r.Visualizzare graficamente lo stato della repository"
	echo "git log"
	echo "##################################################################################################"
	
