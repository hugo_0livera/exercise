package ejb.services.interfaces;


import java.util.List;

public interface ExercisesEJBLocal {

     double calculateSum(Object[] values);


     String[] orderFilteredList(List<String> values, String regExp);


     String[] orderFilteredListTwo(List<String> values, String regExp) ;


     byte[] readPdfFile(String url);
}
