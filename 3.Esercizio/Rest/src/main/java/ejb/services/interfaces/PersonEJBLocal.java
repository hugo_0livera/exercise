package ejb.services.interfaces;

import mybatis.model.Person;

import java.util.List;

public interface PersonEJBLocal {
     Person getPerson(int id);

     void deletePerson(int id) ;

     int createPerson(Person person);

     int updatePerson(Person person);

     List<Person> getAllPersons();

     List <Person> selectByFiscalCode();

     List <Person> selectByFiscalCodeOfPersonMapperExtension();

}
