package ejb.services.implementations;

import ejb.services.interfaces.PersonEJBLocal;
import mybatis.mapper.PersonMapper;
import mybatis.mapper.PersonMapperExtension;
import mybatis.model.Person;
import mybatis.model.PersonExample;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@Local(PersonEJBLocal.class)
@Stateless
public class PersonEJB implements  PersonEJBLocal {

    @Inject
    SqlSessionFactory sqlSessionFactory;

    @Override
    public Person getPerson(int id) {
        SqlSession sqlSession =  sqlSessionFactory.openSession();
         PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
       sqlSession.commit();
       return personMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deletePerson(int id) {
        SqlSession sqlSession =  sqlSessionFactory.openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        personMapper.deleteByPrimaryKey(id);
        sqlSession.commit();
    }

    @Override
    public int createPerson(Person person) {
        SqlSession sqlSession =  sqlSessionFactory.openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        int i =personMapper.insert(person);
        sqlSession.commit();
        return i;
    }

    @Override
    public int updatePerson(Person person) {
        SqlSession sqlSession =  sqlSessionFactory.openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        int i =personMapper.updateByPrimaryKey(person);
        sqlSession.commit();
        return i;
    }

    @Override
    public List<Person> getAllPersons() {
        SqlSession sqlSession =  sqlSessionFactory.openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        PersonExample personExample = new PersonExample();
//        personExample.createCriteria().andFiscalcodeEqualTo("adsa");

//        personMapper.selectByExample(personExample);
        return personMapper.selectByExample(personExample);
    }

    @Override
    public List<Person> selectByFiscalCode() {
        SqlSession sqlSession =  sqlSessionFactory.openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        PersonExample personExample = new PersonExample();
        personExample.createCriteria().andFiscalcodeEqualTo("f");

        //personMapper.selectByExample(personExample);
        return personMapper.selectByExample(personExample);
    }

    @Override
    public List<Person> selectByFiscalCodeOfPersonMapperExtension() {
        SqlSession sqlSession =  sqlSessionFactory.openSession();
        PersonMapperExtension personMapperExtension = sqlSession.getMapper(PersonMapperExtension.class);
        return Arrays.asList(personMapperExtension.selectByFiscalCode("a"));
    }


}
