package ejb.services.implementations;

import ejb.services.interfaces.ExercisesEJBLocal;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Stateless
@Local(ExercisesEJBLocal.class)
public class ExercisesEJB implements ExercisesEJBLocal {

    /**
     * 1. Effettua la somma di tutti i valori in input se numerici
     * @param values
     * @return
     */
    @Override
    public double calculateSum(Object[] values) {
        double sum = 0;
        for(Object object: values) {
            if ( object.getClass() == Integer.class ) {
                double i = (Integer) object;
                sum +=i;
            }else if ( object.getClass() == Double.class ){
                double i = (Double) object;
                sum +=i;
            } else return 0;
        }
        return sum;
    }

    /**
     * 2. Restituisce l'elenco dei valori in input filtrati mediante la regular expression ricevuta in ordine alfabetico decrescente
     * @param values
     * @param regExp
     * @return
     */
    @Override
    public String[] orderFilteredList(List<String> values, String regExp) {
        String[] sorted = values.stream().sorted(Comparator.reverseOrder())
                .filter(p -> p.matches(regExp))
                .toArray(String[]::new);
        return sorted;
    }

    /**
     * 3. Come al punto precedente senza l'utilizzo delle funzionalità di Java 8
     * @param values
     * @param regExp
     * @return
     */
    @Override
    public String[] orderFilteredListTwo(List<String> values, String regExp) {
        Pattern pattern = Pattern.compile(regExp);
        boolean matchFound;
        Matcher matcher;

        Collections.sort(values, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return - o1.compareTo(o2);
            }
        });

        List<String> tmp = new ArrayList<>();

        for(String value: values){
            matcher = pattern.matcher(value);
            matchFound = matcher.find();
            if(matchFound) tmp.add(value);
        }
        String[] sorted = tmp.toArray((new String[0]));
        return sorted;
    }

    /**
     * 4. Legge un file pdf al path specificato e lo restituisce in output
     * @param url
     * @return
     */
    @Override
    public byte[] readPdfFile(String url) {
        File file = new File(url);
        byte[] buffer = new byte[(int)file.length()];

        try {
            FileInputStream fileInputStream = new FileInputStream(url);
            fileInputStream.read(buffer);
            fileInputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  buffer;
    }
}
