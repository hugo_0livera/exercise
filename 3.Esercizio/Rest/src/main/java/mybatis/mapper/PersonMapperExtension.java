package mybatis.mapper;

import mybatis.model.Person;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

public interface PersonMapperExtension {
    @Select({
            "select",
            "ID, NAME, SURNAME, FISCALCODE, AGE",
            "from INT_BANK.PERSON",
            "where FISCALCODE = #{fiscalcode,jdbcType=NUMERIC}"
    })
    @Results({
            @Result(column="ID", property="id", jdbcType= JdbcType.NUMERIC, id=true),
            @Result(column="NAME", property="name", jdbcType=JdbcType.VARCHAR),
            @Result(column="SURNAME", property="surname", jdbcType=JdbcType.VARCHAR),
            @Result(column="FISCALCODE", property="fiscalcode", jdbcType=JdbcType.VARCHAR),
            @Result(column="AGE", property="age", jdbcType=JdbcType.VARCHAR)
    })
    Person selectByFiscalCode(String fiscalcode);
}
