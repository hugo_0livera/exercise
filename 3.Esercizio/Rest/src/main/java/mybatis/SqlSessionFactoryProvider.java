package mybatis;

import java.io.IOException;
import java.io.InputStream;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import mybatis.mapper.PersonMapper;
import mybatis.mapper.PersonMapperExtension;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.cdi.SessionFactoryProvider;


public class SqlSessionFactoryProvider {

	  @Produces
	  @ApplicationScoped
	  @SessionFactoryProvider
	  public SqlSessionFactory produceFactory() throws IOException {
	  	String resource = "mybatis-config.xml";
	    InputStream inputStream = Resources.getResourceAsStream(resource);
	    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
	    sqlSessionFactory.getConfiguration().addMapper(PersonMapper.class);
		sqlSessionFactory.getConfiguration().addMapper(PersonMapperExtension.class);
	    return sqlSessionFactory;
	  }

}
