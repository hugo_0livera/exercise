package com.example.Rest;

import ejb.services.interfaces.PersonEJBLocal;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path  ("/person")
public class Person {

    @Inject
    PersonEJBLocal personEJBLocal;

    /**
     * Get a list of persons
     * @return
     */
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("/allPersons")
    public Response allPersons(){
        try{
            List<mybatis.model.Person> persons =  personEJBLocal.getAllPersons();
            return Response.ok(persons).build();
        }catch (Exception e){
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }


    }

    /**
     * Get a list of persons
     * @return
     */
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("/selectByFiscalCode")
    public Response selectByFiscalCode(){
        try{
            List<mybatis.model.Person> persons =  personEJBLocal.selectByFiscalCode();
            return Response.ok(persons).build();
        }catch (Exception e){
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }


    }

    /**
     * Get a list of persons
     * @return
     */
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("/selectByFiscalCodeExtension")
    public Response selectByFiscalCodeExtension(){
        try{
            List<mybatis.model.Person> persons =  personEJBLocal.selectByFiscalCodeOfPersonMapperExtension();
            return Response.ok(persons).build();
        }catch (Exception e){
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }


    }


    /**
     * Get a list of persons
     * @return
     */
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("{id}")
    public Response getPersons(@PathParam("id")int id){
        try{
            mybatis.model.Person person =  personEJBLocal.getPerson(id);
            return Response.ok(person).build();
        }catch (Exception e){
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }

    }

    /**
     * Delete a person
     * @return
     */
    @DELETE
    @Path("{id}")
    public Response deletePerson(@PathParam("id") int id){
        try{
            personEJBLocal.deletePerson(id);
            return Response.ok().build();
        }catch (Exception e){
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }

    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createPerson(mybatis.model.Person person){
        try{
            if(person.getFiscalcode() == null ) return  Response.status(Response.Status.BAD_REQUEST).build();
            if(person.getFiscalcode().length() < 16 ) return  Response.status(Response.Status.BAD_REQUEST).build();
            int i = personEJBLocal.createPerson(person);
            if(i == 1) return Response.ok("Utente creato con successo").build();
            else return Response.status(404).build();
        }catch (Exception e){
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }


    }


    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updatePerson(mybatis.model.Person person){
        try{
            int i = personEJBLocal.updatePerson(person);
            return Response.ok( i).build();
        }catch (Exception e){
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }

    }

}
