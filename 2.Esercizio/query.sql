/*1.Trovare nome e codice (PLAYERNO) di ogni giocatore il cui nome (campo NAME) inizia con la lettera 'B'. */
SELECT playerno, name 
FROM PLAYERS
WHERE name like 'B%'

/* 2. Trovare il nome e il LEAGUENO per i giocatori il cui LEAGUENO � diverso da 8467 oppure non esiste*/
SELECT name, leagueno
FROM PLAYERS
WHERE leagueno <> 8467

/* 3. Trovare i nomi e le iniziali (campo INNITIALS) dei giocatori che non sono capitani di una squadra (i PLAYERNO dei capitani 
delle squadre sono indicati nella tabella TEAMS)*/
SELECT name, initials
FROM PLAYERS 
WHERE playerno NOT IN 
(
    SELECT playerno
    From TEAMS
)

/* 4, Trovare il codice dei giocatori e il numero totale di sanzioni per tutti i giocatori che hanno ricevuto almeno due
sanzioni*/
SELECT playerno, COUNT (playerno) AS totale_di_sanzioni
FROM PENALTIES 
GROUP BY playerno
HAVING COUNT (playerno) > 1


/* TODO DA VEDERE*/
/* 5. Trovare il codice di tutti i giocatori che hanno ricevuto una multa da L.25 e una multa da L.30*/ 
SELECT playerno
FROM PENALTIES 
WHERE amount = 25 OR amount = 75


/* 6. Trovare il codice di tutte le squadre in cui il giocatore numero 57 non ha mai giocato */
SELECT  DISTINCT teamno
FROM GAMES
WHERE teamno NOT IN
(
    SELECT  teamno
    FROM GAMES
    WHERE playerno = 57
)

/*7. Trovare il nome dei giocatori originari di Inglewood o di Stratford che hanno giocato in almeno 2 squadre */
SELECT g1.playerno, count (*)
FROM GAMES g1 JOIN GAMES g2 ON g1.playerno = g2.playerno and g1.teamno <> g2.teamno
    JOIN PLAYERS p ON (p.playerno = g1.playerno)
WHERE town = 'Inglewood' OR town = 'Stratford'  
GROUP BY g1.playerno
HAVING COUNT (*) > 1

/*8. Trovare il nome, il numero totale di partite vinte e di partite perse  per ogni giocatore che ha vinto almeno 10 partite */
SELECT name, won as totale_partite_vinte, lost as totale_partite_perse
FROM GAMES g JOIN PLAYERS p ON (g.playerno = p.playerno) 
WHERE won >= 10

/*9. Trovare il numero complessivo di partite vinte dai giocatori originari di Stratford che non sono mai stati capitani di una squadra */
SELECT  SUM(won) complessivo_partite_vinte
FROM  PLAYERS p JOIN GAMES g ON (p.playerno= g.playerno)
WHERE town = 'Stratford' AND p.playerno not in 
(
    SELECT playerno
    FROM TEAMS
)

/* TODO Manca la data*/
/*10.Trovare per ogni giocatore che ha ricevuto almeno una sanzione, il codice del giocatore,
la sanzione di importo maggiore e la data in cui la sanzione � stata combinata.*/
SELECT  playerno, MAX(amount) AS importo
FROM PENALTIES  
GROUP BY  playerno
        
/*11. Trovare il codice di tutti i giocatori che hanno giocato in tutte le squadre riportate nella tabella squadre */
SELECT DISTINCT g1.playerno
FROM GAMES g1 JOIN GAMES g2 
ON g1.playerno = g2.playerno and g1.teamno <> g2.teamno

/*12. Trovare il codice di tutti i giocatori che hanno giocato solamente nelle squadre in cui ha giocato il giocatore numero 57 */
SELECT playerno
FROM GAMES 
WHERE playerno NOT IN 
(
    SELECT playerno
    FROM GAMES 
    WHERE teamno NOT IN
    (
        SELECT teamno
        FROM GAMES 
        WHERE playerno = 57
    )
) 

/*13. Trovare il codice di tutti i giocatori che hanno giocato almeno in tutte le squadre in cui ha giocato il giocatore numero 57 */
    SELECT playerno
    FROM GAMES 
    WHERE teamno  IN
    (
        SELECT teamno
        FROM GAMES 
        WHERE playerno = 57
    )

/*TODO: mi sembra la stessa query della domanda 12*/
/*14. Trovare il codice di tutti i giocatori che hanno giocato in tutte e sole le stesse squadre in cui ha giocato il giocatore 57*/


/*15. Trovare il nome e le iniziali dei giocatori di sesso maschile per cui l�ammontare complessivo delle penalita�
pagate e� superiore alla media delle penalita� complessivamente pagate da tutti i giocatori. */
SELECT name,initials, SUM(amount)
FROM PENALTIES pe JOIN PLAYERS pl ON (pe.PLAYERNO = pl.PLAYERNO) 
WHERE SEX = 'M'
GROUP BY name, initials
HAVING SUM(amount) >
(
    SELECT  AVG(amount)
    FROM PENALTIES
)

/*16. Trovare il nome del giocatore e la penalita� minima pagata per i giocatori che hanno ricevuto almeno 2 e non piu� di 4 penalita�, 
e che hanno vinto almeno una partita.  */
SELECT pl.name, MIN(p.amount) as penalit�_minima, count(p.playerno) as numero_di_penalit�
FROM PENALTIES p JOIN GAMES g ON (p.playerno = g.playerno)
    JOIN PLAYERS pl ON (pl.playerno = g.playerno)
WHERE won <> 0
GROUP BY  pl.name
HAVING count (p.playerno) >=2 AND count (p.playerno) <=4

/*17. Per ogni giocatore trovare il nome e la massima penalita� pagata nel 1980.  */
SELECT name, MAX(amount)
FROM PENALTIES pe JOIN PLAYERS pl ON (pe.playerno = pl.playerno) 
WHERE EXTRACT(year from CDATE)= 1980
GROUP BY name


/*18. Trovare il nome dei giocatori per cui l�ammontare delle penalita� pagate nel 1980 e� 
inferiore alla penalita� massima pagata dal giocatore stesso. */

SELECT pla.name
FROM 
(   /*GIOCATORI CHE HANNO PAGATO LA MULTO NEL 1980*/
    SELECT playerno, amount as penalita
    FROM PENALTIES 
    WHERE EXTRACT(year from CDATE)= 1980

) p1 JOIN PLAYERS pla ON (p1.playerno = pla.playerno)
WHERE penalita <= all
(       
        SELECT penalitaMax
        FROM ( /* PENALITA MASSIMA DI TUTTI I GIOCATORI*/
            SELECT playerno, MAX(amount) as penalitaMax
            FROM PENALTIES 
            GROUP BY playerno
        )  p2
        WHERE p2.playerno = p1.playerno
)

/*19. Trovare il nome, il numero totale di partite vinte, il numero totale di partite perse e la media delle penalita�
pagate dai giocatori che hanno preso almeno 2 pena */
SELECT pl.name, SUM(WON) AS partite_vinte, SUM(LOST) AS partite_perse, COUNT(p.playerno) AS numero_di_penalit�, AVG(AMOUNT) media_penalit�
FROM PENALTIES p join GAMES g on (p.PLAYERNO = g.PLAYERNO) 
    JOIN PLAYERS pl on (pl.playerno = g.playerno)
GROUP BY pl.name
HAVING COUNT (p.PLAYERNO) >=2 
